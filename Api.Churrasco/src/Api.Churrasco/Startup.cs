using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Api.Churrasco.Domain.Services;
using System.Reflection;
using System.IO;
using Api.Churrasco.Infrastructure.Mapping;
using Api.Churrasco.Infrastructure.Repositories;
using Api.Churrasco.Infrastructure.Config;
using Api.Churrasco.Domain.Repositories;
using Api.Churrasco.Extensions;

namespace Api.Churrasco
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            DependencyInjection(services);

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Churrasco.Api V1", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger(c => c.RouteTemplate = "swagger/{documentName}/swagger.json");

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Churrasco.Api V1");
            });

            // if (env.IsDevelopment())
            // {
            //     app.UseDeveloperExceptionPage();
            // }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private void DependencyInjection(IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            services.AddSingleton<IServiceProvider>(serviceProvider);

            services.AddConfigClass<IDatabaseConfig, DatabaseConfig>("DatabaseConfig", Configuration);

            services.AddScoped<IChurrascoService, ChurrascoService>();
            services.AddScoped<IDatabaseFileMapping, DatabaseFileMapping>();
            services.AddScoped<IChurrascoRepository, ChurrascoRepository>();
            services.AddScoped<IParticipanteRepository, ParticipanteRepository>();


        }
    }
}
