﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Api.Churrasco.Domain.Models;
using Api.Churrasco.Domain.Services;



namespace Api.Churrasco.Controllers
{
    [ApiController]
    [Route("api/churrasco")]
    public class ChurrascoController : ControllerBase
    {
        private readonly IChurrascoService churrascoService;

        public ChurrascoController(IChurrascoService churrascoService)
        {
            this.churrascoService = churrascoService;
        }

        /// <summary>
        /// Cadastra churrasco
        /// </summary>
        /// <returns>Retorna o churrasco cadastrado</returns>
        /// <response code="200">Churrasco cadastrado</response>
        /// <summary>
        /// Cadastra o churrasco
        /// </summary>
        /// <param name="churrasco"></param>
        /// <returns>Retorna os dados do churrasco cadastrado</returns>        

        [HttpPost]
        [Route("cadastra-churrasco")]
        public List<string> CadastraChurrasco([FromBody] ChurrascoInclusao churrasco)
        {
            return churrascoService.CadastraChurrasco(churrasco);

        }

        /// <summary>
        /// Cadastra participante
        /// </summary>
        /// <returns>Retorna o participante cadastrado</returns>
        /// <response code="200">Participante cadastrado</response>
        /// <summary>
        /// Cadastra o Participante
        /// </summary>
        /// <param name="participante"></param>
        /// <returns>Retorna os dados do participante cadastrado</returns>
        [HttpPost]
        [Route("cadastra-participante")]
        public List<string> CadastraParticipante([FromBody] Participante participante)
        {
            return churrascoService.CadastraParticipante(participante);
        }


        /// <summary>
        /// Consulta churrasco.
        /// </summary>
        /// <returns>Retorna uma lista de parcitipantes e informações do churrasco</returns>
        /// <response code="200">lista de parcitipantes do churrasco</response>
        /// <summary>
        /// Consulta churrasco na data informada
        /// </summary>
        /// <param name="dataChurrasco"></param>
        /// <returns>Retorna as informações do churrasco</returns>

        [HttpGet("consulta-churrasco/{dataChurrasco}")]
        public ChurrascoView ConsultarChurrasco(DateTime dataChurrasco)
        {
            return churrascoService.ConsultaChurrasco(dataChurrasco);
        }

        /// <summary>
        /// Remove participante do churrasco.
        /// </summary>
        /// <returns>Retorna o cpf do participante excluído</returns>
        /// <response code="200">cpf participante</response>
        /// <summary>
        /// Remove o participante do churrasco
        /// </summary>
        /// <param name="cpfParticipante"></param>
        /// <param name="idChurrasco"></param>
        /// <returns>Retorna o cpf do participante removido do churrasco</returns>

        [HttpDelete("remove-participante/{idChurrasco}/{cpfParticipante}")]
        public string RemoveParticipante(string cpfParticipante, long idChurrasco)
        {
            return churrascoService.RemoveParticipante(cpfParticipante, idChurrasco);
        }
    }
}
