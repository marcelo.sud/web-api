using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace Api.Churrasco.Extensions
{
    public static class ConfigExtension
    {
        public static void AddConfigClass<IConfig, TConfig>(this IServiceCollection self, string configSection, IConfiguration configuration)
            where TConfig : IConfig, new()
            where IConfig : class
        {

            var configClass = new TConfig();

            configuration.GetSection(configSection).Bind(configClass);
            self.AddSingleton<IConfig>(configClass);
        }
    }
}
