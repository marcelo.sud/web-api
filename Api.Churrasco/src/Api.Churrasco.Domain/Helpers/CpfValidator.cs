using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Api.Churrasco.Domain.Helpers
{
    public static class CpfValidator
    {
        public static bool Validate(string value)
        {
            if (value != null && Regex.IsMatch(value, "[0-9]{11}"))
            {
                List<int> digitos = value.ToCharArray()
                    .Select(char.ToString)
                    .Select(int.Parse).ToList();

                if (digitos.Distinct().Count() == 1) return false;

                int v1 = (digitos.Take(9).Reverse().Select((di, i) => di * (9 - (i % 10))).Sum() % 11) % 10;
                int v2 = ((digitos.Take(9).Reverse().Select((di, i) => di * (9 - ((i + 1) % 10))).Sum() + v1 * 9) % 11) % 10;
                return (v1 == digitos[9]) && (v2 == digitos[10]);
            }
            else
            {
                return false;
            }
        }
    }
}
