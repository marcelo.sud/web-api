namespace Api.Churrasco.Domain.Helpers
{
    public class ApiResponseOK : ApiResponse
    {
        public object Result { get; }
        public ApiResponseOK(object result)
            : base(200)
        {
            Result = result;
        }

    }
}
