using System;
using System.Collections.Generic;
using Api.Churrasco.Domain.Models;

namespace Api.Churrasco.Domain.Repositories
{
    public interface IChurrascoRepository
    {
        List<string> CadastraChurrasco(ChurrascoInclusao churrasco);
        ChurrascoView ConsultaChurrasco(DateTime dataChurrasco);
        ChurrascoModel ConsultaChurrasco(long idChurrasco);

    }
}
