using System.Collections.Generic;
using Api.Churrasco.Domain.Models;

namespace Api.Churrasco.Domain.Repositories
{
    public interface IParticipanteRepository
    {
        List<string> CadastraParticipante(Participante participante);
        bool ConsultaParticipanteChurrasco(string cpfParticipante, long idChurrasco);
        string RemoveParticipante(string cpfParticipante, long idChurrasco);
    }

}
