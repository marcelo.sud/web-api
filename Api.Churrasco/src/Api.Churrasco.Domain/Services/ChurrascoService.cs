using Api.Churrasco.Domain.Models;
using Api.Churrasco.Domain.Repositories;
using Api.Churrasco.Domain.Helpers;
using System;
using System.Collections.Generic;

namespace Api.Churrasco.Domain.Services
{
    public interface IChurrascoService
    {
        List<string> CadastraChurrasco(ChurrascoInclusao churrascoModel);

        List<string> CadastraParticipante(Participante participante);

        ChurrascoView ConsultaChurrasco(DateTime dataChurrasco);

        string RemoveParticipante(string cpfParticipante, long idChurrasco);
    }
    public class ChurrascoService : IChurrascoService
    {

        private readonly IChurrascoRepository churrascoRepository;
        private readonly IParticipanteRepository participanteRepository;

        public ChurrascoService(IChurrascoRepository churrascoRepository,
                                IParticipanteRepository participanteRepository)
        {
            this.churrascoRepository = churrascoRepository;
            this.participanteRepository = participanteRepository;
        }
        public List<string> CadastraChurrasco(ChurrascoInclusao churrasco)
        {
            if (churrascoRepository.ConsultaChurrasco(churrasco.Data.Date) != null)
                return new List<string> { "Já existe churrasco agendado para a data informada." };

            return this.churrascoRepository.CadastraChurrasco(churrasco);
        }

        public List<string> CadastraParticipante(Participante participante)
        {
            if (!CpfValidator.Validate(participante.Cpf))
                return new List<string> { "Cpf Informado Inválido." };

            if (churrascoRepository.ConsultaChurrasco(participante.IdChurrasco) == null)
                return new List<string> { "Código do churrasco informado inválido." };

            if (participanteRepository.ConsultaParticipanteChurrasco(participante.Cpf, participante.IdChurrasco))
                return new List<string> { "Participante já cadastrado para esse churrasco." };

            return participanteRepository.CadastraParticipante(participante);

        }
        public ChurrascoView ConsultaChurrasco(DateTime dataChurrasco)
        {
            return churrascoRepository.ConsultaChurrasco(dataChurrasco.Date);
        }
        public string RemoveParticipante(string cpfParticipante, long idChurrasco)
        {
            if (!participanteRepository.ConsultaParticipanteChurrasco(cpfParticipante, idChurrasco))
                return "Participante não encontrado.";

            return participanteRepository.RemoveParticipante(cpfParticipante, idChurrasco);
        }
    }
}
