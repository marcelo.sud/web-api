using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Churrasco.Domain.Models
{
    public class ChurrascoModel
    {
        public long Id { get; set; }
        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public string Observacao { get; set; }

    }
}
