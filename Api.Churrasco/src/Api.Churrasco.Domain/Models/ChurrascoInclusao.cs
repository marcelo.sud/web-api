using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Churrasco.Domain.Models
{
    public class ChurrascoInclusao
    {

        /// <summary>
        /// Descrição do churrasco
        /// </summary>
        /// <value></value>        
        [Required(ErrorMessage = "O campo Descrição é obrigatório", AllowEmptyStrings = false)]
        public string Descricao { get; set; }

        /// <summary>
        /// Data do churrasco
        /// </summary>
        /// <value></value>

        [Required(ErrorMessage = "O campo Data é obrigatório")]
        public DateTime Data { get; set; }

        /// <summary>
        /// Observações
        /// </summary>
        /// <value></value>        
        [Required(ErrorMessage = "O campo Observação é obrigatório", AllowEmptyStrings = false)]
        public string Observacao { get; set; }

    }
}
