using System.ComponentModel.DataAnnotations;

namespace Api.Churrasco.Domain.Models
{
    public class Participante
    {

        public long IdChurrasco { get; set; }

        public string Cpf { get; set; }

        public string Nome { get; set; }

        public decimal ValorContribuicao { get; set; }

        public string ComBebida { get; set; }

    }
}
