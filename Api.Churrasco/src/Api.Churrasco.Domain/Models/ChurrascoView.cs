using System;
using System.Collections.Generic;

namespace Api.Churrasco.Domain.Models
{
    public class ChurrascoView
    {
        public ChurrascoView()
        {
            this.Participantes = new List<Participante>();
        }
        public long idChurrasco { get; set; }
        public string DescricaoChurrasco { get; set; }
        public DateTime DataChurrasco { get; set; }
        public string Observacao { get; set; }
        public int QuantidadeParticipes { get; set; }
        public Decimal ValorArrecadado { get; set; }
        public List<Participante> Participantes { get; set; }
    }

}
