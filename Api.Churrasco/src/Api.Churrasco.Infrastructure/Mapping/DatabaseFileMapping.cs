using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Api.Churrasco.Domain.Models;
using Api.Churrasco.Domain.Repositories;
using Api.Churrasco.Infrastructure.Config;

namespace Api.Churrasco.Infrastructure.Mapping
{
    public interface IDatabaseFileMapping
    {
        List<string> Insert(ChurrascoInclusao churrasco);

        List<string> Insert(Participante participante);

        bool SelectAny(string cpfParticipante, long idChurrasco);

        ChurrascoModel Select(long idChurrasco);

        ChurrascoView SelectAll(DateTime dataChurrasco);

        string Delete(string cpfParticipante, long idChurrasco);
    }

    public class DatabaseFileMapping : IDatabaseFileMapping
    {
        private readonly IDatabaseConfig config;
        private string diretorioDados;
        private List<ChurrascoModel> databaseChurrascos = new List<ChurrascoModel>();
        private List<Participante> databaseParticipantes = new List<Participante>();

        public DatabaseFileMapping(IDatabaseConfig config)
        {
            this.config = config;
            CreateDatabaseDirectory();
        }

        private void CreateDatabaseDirectory()
        {
            if (!Directory.Exists(this.config.DiretorioDatabase))
            {
                DirectoryInfo de = Directory.CreateDirectory(this.config.DiretorioDatabase);
            }
            diretorioDados = this.config.DiretorioDatabase;
        }
        private long IdentityDatabaseFile(string fileName)
        {
            long id = 0;

            System.IO.StreamReader file = new System.IO.StreamReader(DataFile(fileName)[0]);
            string line = string.Empty;

            List<string> dataBaseFileList = new List<string>();
            List<string> idList = new List<string>();

            while ((line = file.ReadLine()) != null)
            {
                dataBaseFileList = line.Split(';').ToList();
                idList.Add(dataBaseFileList[0]);
            }

            file.Close();

            id = Convert.ToInt64(idList.Max()) + 1;

            return id;
        }
        private string[] DataFile(string fileName)
        {
            string[] dados = Directory.GetFiles(diretorioDados, fileName, SearchOption.TopDirectoryOnly);

            return dados;
        }

        private string[] DataFiles()
        {
            string[] dados = Directory.GetFiles(diretorioDados, "*.*", SearchOption.TopDirectoryOnly);

            return dados;
        }

        public List<string> Insert(ChurrascoInclusao churrasco)
        {
            long id = 1;

            string diretorioFile = diretorioDados + "Churrasco.dat";

            if (File.Exists(diretorioFile))
                id = IdentityDatabaseFile("Churrasco.dat");

            StreamWriter fileDatabase = new StreamWriter(diretorioFile, true, Encoding.UTF8);

            fileDatabase.WriteLine(id + ";" + churrasco.Descricao + ";" +
                                    churrasco.Data.Date + ";" + churrasco.Observacao);
            fileDatabase.Close();

            List<string> churrascoModel = new List<string>();

            churrascoModel.Add("Id:" + id.ToString());
            churrascoModel.Add("Descrição:" + churrasco.Descricao);
            churrascoModel.Add("Data:" + churrasco.Data);
            churrascoModel.Add("Observação:" + churrasco.Observacao);

            return churrascoModel;

        }

        public List<string> Insert(Participante participante)
        {
            string diretorioFile = diretorioDados + "Participante.dat";

            StreamWriter fileDatabase = new StreamWriter(diretorioFile, true, Encoding.UTF8);

            fileDatabase.WriteLine(participante.IdChurrasco + ";" + participante.Cpf + ";" +
                                    participante.Nome + ";" + participante.ValorContribuicao.ToString().Replace(',', '.') + ";" + participante.ComBebida);
            fileDatabase.Close();

            List<string> participanteModel = new List<string>();

            participanteModel.Add("IdChurrasco:" + participante.IdChurrasco.ToString());
            participanteModel.Add("Cpf:" + participante.Cpf);
            participanteModel.Add("Nome:" + participante.Nome);
            participanteModel.Add("Valor Contribuição:" + participante.ValorContribuicao);
            participanteModel.Add("Com Bebida:" + participante.ComBebida);


            return participanteModel;
        }

        public bool SelectAny(string cpfParticipante, long idChurrasco)
        {
            string diretorioFile = diretorioDados + "Participante.dat";

            if (!File.Exists(diretorioFile))
                return false;

            System.IO.StreamReader file = new System.IO.StreamReader(DataFile("Participante.dat")[0]);
            string line = string.Empty;
            string cpf = string.Empty;
            long id = 0;

            List<string> dataBaseFileList = new List<string>();
            Participante participante = new Participante();

            while ((line = file.ReadLine()) != null)
            {
                dataBaseFileList = line.Split(';').ToList();

                id = Convert.ToInt64(dataBaseFileList[0]);

                cpf = dataBaseFileList[1];

                if (cpfParticipante.Equals(cpf) && id.Equals(idChurrasco))
                {
                    file.Close();
                    return true;
                }
            }

            file.Close();

            return false;
        }


        public ChurrascoModel Select(long id)
        {
            string diretorioFile = diretorioDados + "Churrasco.dat";

            if (!File.Exists(diretorioFile))
                return null;

            System.IO.StreamReader file = new System.IO.StreamReader(DataFile("Churrasco.dat")[0]);
            string line = string.Empty;

            List<string> dataBaseFileList = new List<string>();
            ChurrascoModel churrasco = new ChurrascoModel();
            long idChurrasco = 0;

            while ((line = file.ReadLine()) != null)
            {
                dataBaseFileList = line.Split(';').ToList();

                idChurrasco = Convert.ToInt64(dataBaseFileList[0]);

                if (idChurrasco.Equals(id))
                {
                    churrasco.Id = idChurrasco;
                    churrasco.Descricao = dataBaseFileList[1];
                    churrasco.Data = Convert.ToDateTime(dataBaseFileList[2]).Date;

                    file.Close();
                    return churrasco;
                };
            };

            file.Close();

            return churrasco;
        }

        public ChurrascoView SelectAll(DateTime data)
        {
            DateTime dataChurrasco;
            ChurrascoView churrasco = new ChurrascoView();
            List<Participante> participantes = new List<Participante>();

            long idChurrasco = 0;

            foreach (string datafile in DataFiles())
            {
                System.IO.StreamReader file = new System.IO.StreamReader(datafile);
                string line = string.Empty;

                List<string> dataBaseFileList = new List<string>();

                while ((line = file.ReadLine()) != null)
                {
                    dataBaseFileList = line.Split(';').ToList();

                    if (Path.GetFileName(datafile).Equals("Churrasco.dat"))
                    {
                        dataChurrasco = Convert.ToDateTime(dataBaseFileList[2]).Date;

                        if (dataChurrasco.Equals(data))
                        {
                            idChurrasco = Convert.ToInt64(dataBaseFileList[0]);

                            if (dataChurrasco.Equals(data.Date))
                            {
                                churrasco.idChurrasco = Convert.ToInt64(dataBaseFileList[0]);
                                churrasco.DescricaoChurrasco = dataBaseFileList[1];
                                churrasco.DataChurrasco = Convert.ToDateTime(dataBaseFileList[2]).Date;
                                churrasco.Observacao = dataBaseFileList[3];
                            }
                        }
                    }
                    else
                    {
                        if (idChurrasco.Equals(Convert.ToInt64(dataBaseFileList[0])))
                        {
                            participantes.Add(new Participante
                            {
                                IdChurrasco = Convert.ToInt64(dataBaseFileList[0]),
                                Cpf = dataBaseFileList[1],
                                Nome = dataBaseFileList[2],
                                ValorContribuicao = Convert.ToDecimal(dataBaseFileList[3]),
                                ComBebida = dataBaseFileList[4]
                            });
                        }
                    }
                }

                file.Close();
            }

            if (idChurrasco == 0) return null;


            churrasco.Participantes = participantes;
            churrasco.QuantidadeParticipes = participantes.Count();
            churrasco.ValorArrecadado = participantes.Sum(participantes => participantes.ValorContribuicao);

            return churrasco;
        }

        public string Delete(string cpfParticipante, long idChurrasco)
        {
            System.IO.StreamReader file = new System.IO.StreamReader(DataFile("Participante.dat")[0]);
            string line = string.Empty;

            List<string> dataBaseFileList = new List<string>();
            List<Participante> participantes = new List<Participante>();
            Participante participantesRemove = new Participante();

            while ((line = file.ReadLine()) != null)
            {
                dataBaseFileList = line.Split(';').ToList();

                participantes.Add(new Participante
                {
                    IdChurrasco = Convert.ToInt64(dataBaseFileList[0]),
                    Cpf = dataBaseFileList[1],
                    Nome = dataBaseFileList[2],
                    ValorContribuicao = Convert.ToDecimal(dataBaseFileList[3]),
                    ComBebida = dataBaseFileList[4]
                });
            };

            file.Close();

            System.IO.File.Delete(diretorioDados + "Participante.dat");

            int participantesidx = participantes.FindIndex(p => p.Cpf == cpfParticipante && p.IdChurrasco == idChurrasco);
            participantes.RemoveAt(Convert.ToInt32(participantesidx));

            foreach (Participante participante in participantes)
                Insert(participante);

            return cpfParticipante;
        }
    }
}
