namespace Api.Churrasco.Infrastructure.Config
{
    public interface IDatabaseConfig
    {
        string DiretorioDatabase { get; set; }
    }
    public class DatabaseConfig : IDatabaseConfig
    {
        public string DiretorioDatabase { get; set; }

    }

}
