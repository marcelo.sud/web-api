using System;
using System.Collections.Generic;
using Api.Churrasco.Domain.Models;
using Api.Churrasco.Domain.Repositories;
using Api.Churrasco.Infrastructure.Mapping;

namespace Api.Churrasco.Infrastructure.Repositories
{

    public class ChurrascoRepository : IChurrascoRepository
    {
        private readonly IDatabaseFileMapping databaseFileMapping;

        public ChurrascoRepository(IDatabaseFileMapping databaseFileMapping)
        {
            this.databaseFileMapping = databaseFileMapping;
        }
        public List<string> CadastraChurrasco(ChurrascoInclusao churrascoInclusao)
        {
            return databaseFileMapping.Insert(churrascoInclusao);
        }
        public ChurrascoView ConsultaChurrasco(DateTime dataChurrasco)
        {
            return databaseFileMapping.SelectAll(dataChurrasco);
        }
        public ChurrascoModel ConsultaChurrasco(long idChurrasco)
        {
            return databaseFileMapping.Select(idChurrasco);
        }
    }
}
