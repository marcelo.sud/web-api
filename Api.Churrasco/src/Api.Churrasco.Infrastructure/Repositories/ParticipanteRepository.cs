using System.Collections.Generic;
using Api.Churrasco.Domain.Models;
using Api.Churrasco.Domain.Repositories;
using Api.Churrasco.Infrastructure.Mapping;

namespace Api.Churrasco.Infrastructure.Repositories
{
    public class ParticipanteRepository : IParticipanteRepository
    {
        private readonly IDatabaseFileMapping databaseFileMapping;

        public ParticipanteRepository(IDatabaseFileMapping databaseFileMapping)
        {
            this.databaseFileMapping = databaseFileMapping;
        }
        public List<string> CadastraParticipante(Participante participante)
        {
            return databaseFileMapping.Insert(participante);
        }
        public bool ConsultaParticipanteChurrasco(string cpfParticipante, long idChurrasco)
        {
            return databaseFileMapping.SelectAny(cpfParticipante, idChurrasco);
        }
        public string RemoveParticipante(string cpfParticipante, long idChurrasco)
        {
            return databaseFileMapping.Delete(cpfParticipante, idChurrasco);
        }
    }
}
