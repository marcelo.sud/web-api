# Descrição

Tem como objetivo o gerenciamento de churrascos.

### Requisitos para instalação

*  Instalação do Microsoft .NET Core SDK 3.1.201

### Arquivo de Configuração
```
{
  "DatabaseConfig": {
    "DiretorioDatabase": "C:\\dataBase\\"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}
```

## Execução da API como desenvolvedor

1.  Após configurar o ambiente, clone a aplicação no GitLab.
2.  Abra com o visual code ou visual studio community a pasta extraída.
3.  Para executar apenas de o comando **CTRL+F5** que verá todos os comandos que estão sendo executados por baixo.

Comandos implícitos feitos ao pressionar CTRL+F5

```
dotnet restore
dotnet build
dotnet run```

## Observações
 A Api utiliza um Database File para a geração Mock, simulando o armazenamento de dados (ver: "DiretorioDatabase": "C:\\dataBase\\""),caso não exista 
 será criado automaticamente pela aplicação.
 **Nessa primeira versão a Api não está desenvolvida com acesso a banco de dados.
 
 **A Api pode ser testada utilizando o Swagger.
